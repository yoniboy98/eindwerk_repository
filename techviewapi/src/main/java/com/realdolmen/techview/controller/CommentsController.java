/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.com.realdolmen.techview.controller;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import src.com.realdolmen.techview.beans.Comments;
import src.com.realdolmen.techview.dao.CommentsDao;

import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.RollbackException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author YVDBL89
 */
@RestController
public class CommentsController {


    @Autowired
 private CommentsDao commentdao;

/**Generate rescource so I can access my comments on id in my React-native. */
    @GetMapping("/api/comments/id")
    public Comments getById(
            @RequestParam(name = "id", required = true) long id
    ) {
        return commentdao.findById(id);
    }


    /**Generate resource so I can access my comments on productId in my React-native. */
    @GetMapping("/api/comments/productId")
    public List<Comments> getByProductId (
            @RequestParam(name = "productId", required = true) String productId
    ) {
        return commentdao.findByProductId(productId);
    }

    /**Generate rescource so I can access all my comments in my React-native. */
    @GetMapping("/api/comments/all")
    public List<Comments> allCommentsOnProductId(
    ) {
        List<Comments> comment = new ArrayList<>();
        comment = commentdao.findAll();
        return comment;
    }






/*
    @PostMapping("/api/comments/add")
    public boolean addComment (
            @RequestParam(name = "add", required = true) Comments comment
    ) {
     return commentdao.add(comment);
    }

    @DeleteMapping("/api/comments/id")
    public ResponseEntity<Void> deleteCommentById(@RequestParam(name = "id") int id) {
        try {
            commentdao.delete(id);
            return ResponseEntity.ok().build();
        } catch (RollbackException ex) {
                        return ResponseEntity.notFound().build();
        }
    }*/


}