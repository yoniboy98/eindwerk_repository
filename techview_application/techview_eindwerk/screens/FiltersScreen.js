import React, {useState, useEffect, useCallback} from 'react';
import {HeaderButtons, Item} from 'react-navigation-header-buttons';
import HeaderButton from '../components/HeaderButton';
import {View, Text, Switch, Platform, StyleSheet, Alert, ScrollView} from 'react-native';
import {useDispatch} from 'react-redux';
import {setFilters} from '../store/actions/products';
import Colors from '../constants/Colors';


/**Makes the user toggle on a boolean value with false or true.
 * That value will look for whats available in the json file with the specify filters. */
const FilterSwitch = props => {
    return (
        <View style={styles.filterContainer}>
            <Text>{props.label}</Text>
            <Switch
                trackColor={{true: Colors.primaryColor}}
                thumbColor={Platform.OS === 'android' ? Colors.primaryColor : ''}
                value={props.state}
                onValueChange={props.onChange}
            />
        </View>
    );
};

const FiltersScreen = props => {
    const {navigation} = props;
    const [isAvailable, setIsAvailable] = useState(false);
    const [hasProcessor, setIsProcessor] = useState(false);
    const [hasStorage, setIsStorage] = useState(false);
    const [hasSoftware, setIsSoftware] = useState(false);
    const [isMaxPrice, setMaxPrice] = useState(false);
    const [isMinPrice, setMinPrice] = useState(false);
    const [isDescription, setIsDescription] = useState(false);


    const dispatch = useDispatch();

    /**Method for saving the filters */
    const saveFilters = useCallback(() => {
            const appliedFilters = {
                available: isAvailable,
                processor: hasProcessor,
                storage: hasStorage,
                software: hasSoftware,
                maxPrice: isMaxPrice,
                minPrice: isMinPrice,
                description: isDescription


            };

            dispatch(setFilters(appliedFilters));
        },
        [isAvailable, hasProcessor, hasSoftware, hasStorage, isMaxPrice, isMinPrice, isDescription, dispatch]);

    /**Filter will be saved when user press the save icon */
    useEffect(() => {
        navigation.setParams({save: saveFilters});
    }, [saveFilters]);

    /** All switch options for filter products out of the list*/
    return (
        <ScrollView>
            <View style={styles.screen}>
                <Text style={styles.title}>Press to activate</Text>
                <Text style={{fontSize: 11, color: 'grey', marginTop: -5}}>(This will not work when you make use of the searchbar.)</Text>
                <FilterSwitch
                    label=' Filter products that are not available '
                    state={isAvailable}
                    onChange={newValue => {
                        setIsAvailable(newValue)

                    }}
                />
                <FilterSwitch
                    label=' Filter products with no processor'
                    state={hasProcessor}
                    onChange={newValue => {
                        setIsProcessor(newValue)

                    }}
                />
                <FilterSwitch
                    label=' Filter products with no storage'
                    state={hasStorage}
                    onChange={newValue => {
                        setIsStorage(newValue)

                    }}
                />
                <FilterSwitch
                    label='Filter products with no description'
                    state={isDescription}
                    onChange={newValue => {
                        setIsDescription(newValue)

                    }}
                />
                <FilterSwitch
                    label=' Filter products with no software'
                    state={hasSoftware}
                    onChange={newValue => {
                        setIsSoftware(newValue)

                    }}
                />
                <FilterSwitch
                    label='Maximum price is not more then €1000'
                    state={isMaxPrice}
                    onChange={newValue => {
                        setMaxPrice(newValue)

                    }}
                />

                <FilterSwitch
                    label='Minimum price is more then €500'
                    state={isMinPrice}
                    onChange={newValue => {
                        setMinPrice(newValue)

                    }}
                />


            </View>
        </ScrollView>
    );
};

/**Send alert to the user so the user will know that they can't forget to push the save button */
const showAlert = () => {
    Alert.alert(
        'Do not forget to press the save icon after switching from filter !'
    )

}


/**Set an option Icon in the left top of the header to access different screens.
 *On the right top a save Icon so that the user now that it needs to be saved. */
FiltersScreen.navigationOptions = navData => {

    return {
        headerTitle: 'Filters',
        headerLeft: (
            <HeaderButtons HeaderButtonComponent={HeaderButton}>
                <Item
                    title='Menu'
                    iconName='ios-menu'
                    onPress={() => {
                        navData.navigation.toggleDrawer()
                    }}
                />
            </HeaderButtons>
        ),
        headerRight: (
            <HeaderButtons HeaderButtonComponent={HeaderButton}>
                <Item
                    title='Save'
                    iconName='ios-save'
                    onPress={navData.navigation.getParam('save') || showAlert()
                    }

                />
            </HeaderButtons>

        )

    };

};


const styles = StyleSheet.create({
    screen: {
        paddingTop: 20,
        flex: 1,
        alignItems: 'center'
    },
    title: {
        fontFamily: 'open-sans-bold',
        fontSize: 22,
        margin: 20,
        textAlign: 'center'
    },
    filterContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '80%',
        marginVertical: 10
    }
});

/**Export my class so it can be called in other classes. */
export default FiltersScreen;