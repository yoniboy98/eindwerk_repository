import React, {useEffect, useCallback} from 'react';
import {View, ScrollView, Text, StyleSheet, ImageBackground} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {HeaderButtons, Item} from 'react-navigation-header-buttons';
import {toggleFavorite} from '../store/actions/products';
import HeaderButton from '../components/HeaderButton';
import {Container, Content, List, ListItem} from "native-base";
import BuyButton from "../components/BuyButton";
import CommentButton from "../components/CommentButton";


const DetailScreen = props => {

    /** Search for available products*/
    const availableProducts = useSelector(state => state.products.products);

    /** Get the productId from products */
    const productId = props.navigation.getParam('productId');


    /**See if a product has been added to your wish list */
    const isFavoriteProduct = useSelector(state => state.products.favoriteProducts.some(product => product.id === productId));

    /** Search by product id and look for what is available and create a variable where I can access all my products data from (selected). */
    const selected = availableProducts.find(product => product.id === productId);

    const dispatch = useDispatch();

    /**Callback function that let the user toggle on heart icon. */
    const toggleFavoriteHandler = useCallback(() => {
        dispatch(toggleFavorite(productId));
    }, [dispatch, productId]);

    /** Set the latest state of favorites*/
    useEffect(() => {
        props.navigation.setParams({toggleFav: toggleFavoriteHandler});
    }, [toggleFavoriteHandler]);

    useEffect(() => {
        props.navigation.setParams({isFav: isFavoriteProduct});
    }, [isFavoriteProduct]);

    /**Show all the details of a product in a native base list with the use of (selector)
     * Comment button that will navigate on id to CommentScreen.
     * Buy button that will navigate to the specific URL link of Bol.com*/
    return (
        <ScrollView>
            <Container style={{height: '100%'}}>
                <Content>
                    <List style={styles.container}>
                        <ImageBackground source={{uri: selected.imageUrl}} style={styles.image}>
                        </ImageBackground>
                        <ListItem itemDivider>
                            <Text style={styles.textHeader}>Name</Text>
                        </ListItem>
                        <ListItem>
                            <Text>{selected.productName}</Text>
                        </ListItem>

                        <ListItem itemDivider>
                            <Text style={styles.textHeader}>Processor</Text>
                        </ListItem>
                        <ListItem>
                            <Text>{selected.processor}</Text>
                        </ListItem>

                        <ListItem itemDivider>
                            <Text style={styles.textHeader}>Storage</Text>
                        </ListItem>
                        <ListItem>
                            <Text>{selected.storage}</Text>
                        </ListItem>

                        <ListItem itemDivider>
                            <Text style={styles.textHeader}>Software</Text>
                        </ListItem>
                        <ListItem>
                            <Text>{selected.software}</Text>
                        </ListItem>


                        <ListItem itemDivider>
                            <Text style={styles.textHeader}>Description</Text>
                        </ListItem>
                        <ListItem>
                            <Text>{selected.description}</Text>
                        </ListItem>

                        <ListItem itemDivider>
                            <Text style={styles.textHeader}>Price</Text>
                        </ListItem>
                        <ListItem>
                            <Text>min €{selected.minPrice} - max €{selected.maxPrice}</Text>
                        </ListItem>

                        <ListItem itemDivider>
                            <Text style={styles.textHeader}>Size</Text>
                        </ListItem>
                        <ListItem>
                            <Text>{selected.size}</Text>
                        </ListItem>


                        <View style={styles.buttonContainer}>
                            <CommentButton onPress={() => {
                                props.navigation.navigate({
                                    routeName: "Comments", params: {
                                        productId: selected.id,
                                        smallName: selected.smallName

                                    }
                                });
                            }}/>
                            <BuyButton
                                link={selected.buyLink}/>

                        </View>


                    </List>
                </Content>
            </Container>
        </ScrollView>
    );
};


/**Heart icon in the right top that I use to toggle the user between in wish list or not.
 * also display the short name of my product in the header.
 */
DetailScreen.navigationOptions = (navigationData) => {
    const productTitle = navigationData.navigation.getParam('smallName');
    const toggleFavorite = navigationData.navigation.getParam('toggleFav');
    const isFavorite = navigationData.navigation.getParam('isFav');
    return {
        headerTitle: productTitle,
        headerRight: (
            <HeaderButtons HeaderButtonComponent={HeaderButton}>
                <Item
                    title='Favorite'
                    iconName={isFavorite ?
                        'ios-heart' :
                        'ios-heart-empty'}
                    onPress={toggleFavorite}
                />
            </HeaderButtons>
        )
    };
};

const styles = StyleSheet.create({
    image: {
        width: '100%',
        height: 200
    },
    details: {
        flexDirection: 'row',
        padding: 15,
        justifyContent: 'space-around'
    },
    textHeader: {
        fontFamily: 'open-sans-bold',
        fontSize: 22,
        textAlign: 'center'
    },
    listItem: {
        marginVertical: 10,
        marginHorizontal: 20,
        borderColor: '#ccc',
        borderWidth: 1,
        padding: 10
    },
    buttonContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: 30,
        paddingBottom: 20,
        marginLeft: '5%',
        marginRight: '5%'


    }
});

/**Export my class so it can be called in other classes. */
export default DetailScreen;