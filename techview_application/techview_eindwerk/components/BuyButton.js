import React from 'react';
import { Linking,TouchableOpacity,View} from 'react-native';
import {  Text } from 'native-base';
import { Ionicons } from '@expo/vector-icons';
import AwesomeButtonCartman  from 'react-native-really-awesome-button/src/themes/cartman';

/** my button component */
const BuyButton = props => {
    return(
<TouchableOpacity >

        <AwesomeButtonCartman style={{paddingBottom: 12}}
              width={130}
                onPress={() => Linking.openURL(props.link)}>

            <Text><Ionicons name="md-cart" size={27}/>  BUY   </Text></AwesomeButtonCartman>

</TouchableOpacity>
    );
};



export default BuyButton;