import React from 'react';
import {ScrollView, StyleSheet, Text, TextInput, View} from "react-native";
import colors from "../constants/Colors";


/** My search component.
 * No auto correct.
 * autofocus is on so the keyboard will now pop up automatically.
 * Set placeholder and let the user now witch search options he/she has.
  */

const SearchItem = props => {

return (
    <View style={{backgroundColor: colors.accentColor,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'}} >

        <Text style ={styles.title}>Search For Tech</Text>


        <TextInput style={ styles.searchTextInput}
                   placeholder="name, storage, price..."
                   autoCorrect={false}
                   autoFocus={true}
                   {...props}

            />


    </View>

);
}


const styles = StyleSheet.create({

    title: {
        color: "black",
        fontSize: 32,
        fontWeight: '700',
        textAlign: 'center',
        marginBottom: 20,
        paddingTop: 25
    },
    searchTextInput: {
        fontSize: 20,
        fontWeight: '300',
        padding: 20,  width: 300,
        borderRadius: 8,
textAlign: 'center',
        color: 'white',
        borderWidth: 2,
backgroundColor: 'black'

    },


});

export default SearchItem;