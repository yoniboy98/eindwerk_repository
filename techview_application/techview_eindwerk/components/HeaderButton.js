import React from 'react';
import { HeaderButton } from 'react-navigation-header-buttons';
import { Ionicons } from '@expo/vector-icons';


/** My headerButtons component, I can later specify witch icon I will use. */
const CustomHeaderButton = props => {
    return (
        <HeaderButton
            {...props}
            IconComponent={Ionicons}
            iconSize={30}
            color={ 'gold'}
        />
    );
};

export default CustomHeaderButton;
