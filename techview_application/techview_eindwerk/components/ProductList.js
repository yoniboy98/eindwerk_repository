import React from 'react';
import { View, FlatList, StyleSheet } from 'react-native';
import { useSelector } from 'react-redux';
import ProductItem from './ProductItem';



/** My productList component
 * Set favorite value.
 * */
const ProductList = props => {
    const favoriteProduct = useSelector(state => state.products.favoriteProducts);

    const renderProductItem = itemData => {
        const isFavorite = favoriteProduct.some(product => product.id === itemData.item.id);


        return (
            <ProductItem
                productId={itemData.item.id}
                smallName={itemData.item.smallName}
                productName={itemData.item.productName}
                image={itemData.item.imageUrl}
                processor={itemData.item.processor}
                storage={itemData.item.storage}
                software={itemData.item.software}
                description={itemData.item.description}
                size={itemData.item.size}
                minPrice={itemData.item.minPrice}
                maxPrice={itemData.item.maxPrice}
                buyLink={itemData.item.buyLink}
                onSelectProduct={() => {
                    props.navigation.navigate({
                        routeName: 'Detail',
                        params: {
                            productId: itemData.item.id,
                            smallName: itemData.item.smallName,
                            isFav: isFavorite
                        }
                    });
                }}
            />
        );
    };

    /**Show all data from products. */
    return (
        <View style={styles.listContainer}>
            <FlatList
                data={props.listData}
                keyExtractor={(item, index) => item.id}
                renderItem={(item) => renderProductItem(item)}
                style={styles.list}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    listContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    list: {
        width: '100%'
    }
});

export default ProductList;