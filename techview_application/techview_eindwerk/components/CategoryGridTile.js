import React from 'react';
import { View, Text, TouchableOpacity,ImageBackground } from 'react-native';
import DefaultStyle from "../constants/default-style";



/** My Categories component*/
const CategoryGridTile = props => {
    return (
        <TouchableOpacity   onPress={props.onSelect}>
            <ImageBackground source={{uri: props.photoLink}} style={DefaultStyle.catoImage}

            >
                <View style={DefaultStyle.gridContainer}>

                    <Text style={DefaultStyle.textStyle}
                        NumberOfLines={2}>
                        {props.title}
                    </Text>

                </View>

            </ImageBackground>
        </TouchableOpacity>
    );
};




export default CategoryGridTile;