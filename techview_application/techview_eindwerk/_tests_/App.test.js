import React from 'react'
import {shallow} from 'enzyme';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import renderer from 'react-test-renderer';
import * as SET_FILTERS from '../store/actions/products';
import * as TOGGLE_FAVORITE from '../store/actions/products';
import selectReducer from '../store/reducers/products';


test('Test the test', () => {
    expect(2 + 2).toBe(4);
});

describe('INITIAL_STATE favorites', () => {
    test('is correct', () => {
        const action = { type: 'TOGGLE_FAVORITE' };
        expect(selectReducer(undefined, action)).toMatchSnapshot();
    });
});

describe('INITIAL_STATE filters', () => {
    test('is correct to', () => {
        const action = { filters: 'SET_FILTERS' };
        expect(selectReducer(false, action)).toMatchSnapshot();
    });
});


const popular = require('../assets/POPULARPRODUCTS');
const pro = require('../assets/PRODUCTS');
test('popular object assignment 1', () => {
    expect(popular[0]).toEqual(pro[1]);
});

test('popular object assignment 2', () => {
    expect(popular[1]).toEqual(pro[4]);
});

test('popular object assignment 3', () => {
    expect(popular[2]).toEqual(pro[9]);
});

test('there is no U in Techview', () => {
    expect('Techview').not.toMatch(/U/);
});

function compileAndroidCode() {
    throw new Error('you are using the wrong JDK');
}
test('compiling android goes as expected', () => {
    expect(compileAndroidCode).toThrow();
    expect(compileAndroidCode).toThrow(Error);

    // You can also use the exact error message or a regexp
    expect(compileAndroidCode).toThrow('you are using the wrong JDK');
    expect(compileAndroidCode).toThrow(/JDK/);
});



test('is not zero', () => {
    for (let a = 1; a < 10; a++) {
        for (let b = 1; b < 10; b++) {
            expect(a + b).not.toBe(0);
        }
    }
});


test('test', () => {
    const value = 2 + 2;
    expect(value).toBeGreaterThan(3);
    expect(value).toBeGreaterThanOrEqual(3.5);
    expect(value).toBeLessThan(5);
    expect(value).toBeLessThanOrEqual(4.5);

    // toBe and toEqual are equivalent for numbers
    expect(value).toBe(4);
    expect(value).toEqual(4);
});




const wait = jest.fn();

test('wait', () => {
    expect(wait).toHaveBeenCalledTimes(0);
});










/*
const HeaderButton = require('../components/HeaderButton');

describe('MyComponent', () => {
    it('should render correctly HeaderButton', () => {
        const component = shallow(<HeaderButton/>);

        expect(component).toMatchSnapshot();
    });
});
*/

/*it('render correctly text component', () => {
    const Buybutton = Renderer.create(<BuyButton />).toJSON();
    expect(Buybutton).toMatchSnapshot();
});*/

