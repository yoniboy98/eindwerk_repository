import {StyleSheet} from 'react-native';
import Colors from '../constants/Colors';

/** This class can be called to put all my styling at one place.*/
export default StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',

    },
    gridItem: {
        flex: 1,
        height: 100,
        width: '100%',
        borderRadius: 10,
        overflow: 'hidden'
    },
    gridContainer: {},
    title: {
        fontSize: 18,
        fontFamily: 'open-sans-bold',
    },
    productTitle: {
        color: 'black',
        padding: 5,
        textAlign: 'center'
    },

    productItem: {

        height: 250,
        width: '100%',
        borderRadius: 20,
        overflow: 'hidden',
        paddingTop: 15,
    },
    productItemPopular: {
        borderWidth: 5,
        borderColor: 'gold',
        height: 300,
        width: '100%',
        borderRadius: 20,
        overflow: 'hidden',
        marginBottom: 25
    },
    productRow: {
        flexDirection: 'row',

    },
    productHeader: {
        height: '85%',

    },
    productHeight: {
        height: 200
    },
    titleContainer: {
        backgroundColor: "darkgrey",
        opacity: 0.7,


    },
    productDetail: {
        height: '30%',
        backgroundColor: Colors.primary,
        justifyContent: 'space-between',
        flexDirection: 'column'

    },
    bgImage: {
        width: '100%',
        justifyContent: 'flex-start',

    },
    catoImage: {
        width: '100%',
        justifyContent: 'flex-end',
        height: 120,


    },
    textStyle: {
        fontSize: 30,
        fontWeight: 'bold',
        color: '#1c1c1c',
        backgroundColor: "#ffffff80"

    },
    CommentBox: {
        height: 150,
        backgroundColor: "whitesmoke",
        borderWidth: 3,
        paddingLeft: 5,
        paddingBottom: 150


    }


})
