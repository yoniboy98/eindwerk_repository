import {TOGGLE_FAVORITE, SET_FILTERS} from '../actions/products';


/** Call the PRODUCTS json file. */
let PRODUCTS = require("../../assets/PRODUCTS");


/** initial states*/
const initialState = {
    products: PRODUCTS,
    filteredProducts: PRODUCTS,
    favoriteProducts: []
};

/**Action for adding a product to favorites on id.
 * Take the next state, compare it with the previous state.
 * Set an action to it.
 * If product is favorite, set product in wish list
 * */
const productReducer = (state = initialState, action) => {
    switch (action.type) {
        case TOGGLE_FAVORITE:
            const existingIndex = state.favoriteProducts.findIndex(product => product.id === action.productId);
            if (existingIndex >= 0) {

                const updatedFavoriteProducts = [...state.favoriteProducts];
                updatedFavoriteProducts.splice(existingIndex, 1);
                return {...state, favoriteProducts: updatedFavoriteProducts};
            } else {

                const product = state.products.find(product => product.id === action.productId)
                return {...state, favoriteProducts: state.favoriteProducts.concat(product)};
            }


        /** Filter options in products with boolean values and update the productList.*/
        case SET_FILTERS:
            const Filters = action.filters;
            const updatedFilteredProducts = state.products.filter(product => {
                if (Filters.available && !product.available) {
                    return false;
                }
                if (Filters.processor && product.processor === "/") {
                    return false;
                }
                if (Filters.storage && product.storage === "/") {
                    return false;
                }
                if (Filters.software && product.software === "/") {
                    return false;
                }
                if (Filters.maxPrice && product.maxPrice > 1000) {
                    return false;
                }
                if (Filters.minPrice && product.minPrice < 500) {
                    return false;
                }
                if (Filters.description && product.description === "/") {
                    return false;
                }


                return true;


            });
            return {...state, filteredProducts: updatedFilteredProducts};
        default:
            return state;
    }
};

export default productReducer;